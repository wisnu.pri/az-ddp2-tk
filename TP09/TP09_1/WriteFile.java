

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;

public class WriteFile {
    private FileWriter csvWriter;
    private String fileName;
    // Pembuatan file
    public WriteFile(String fileName, String header) throws IOException {
        try {
            csvWriter = new FileWriter(fileName);
            csvWriter.append(header);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.fileName = fileName;
        ReadFile fileBaru = new ReadFile(fileName);
    }
    //Penggabungan 2 files menjadi 1 file baru
    public void combineFiles(ArrayList<String[]> dataCust, ArrayList<String[]> transaksi) throws IOException,
            NullPointerException {
        try {
            for (int i = 0; i < dataCust.size(); i++) {
                String[] data = dataCust.get(i);
                String id = data[0];
                for (int j = 0; j < transaksi.size(); j++) {
                    String[] history = transaksi.get(j);
                    if(id.equals(history[0])){
                        String first = String.join(",", data[0],data[1]);
                        String second = String.join(",", history[1],history[2]);
                        csvWriter.append(String.join(",", first,second,"\n"));
                    }
                }
            }
            csvWriter.flush();
        } catch (NullPointerException e) {
            throw new NullPointerException("Ada yang objek yang tidak berbentuk");
        }
        System.out.println(fileName+" berhasil dibuat");
    }
    //Penambahan record pada file baru
    public void addRecord(ArrayList<String> lookup ) throws IOException,InputMismatchException {
        try {
            String theId = lookup.get(0);
            String theName = lookup.get(1);
            String theDate = lookup.get(2);
            String thePrice = lookup.get(3);

            csvWriter.append(String.join(",", theId, theName, theDate, thePrice, "\n"));
        } catch (InputMismatchException e) {
            throw new InputMismatchException("Terdapat kesalahan format input!");
        }    
    }

    public void closeFile() throws IOException {
        csvWriter.close();
    }
}
