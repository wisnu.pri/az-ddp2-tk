

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import javax.xml.crypto.Data;

public class ReadFile {
    private ArrayList<String[]> temp = new ArrayList<String[]>();
    private ArrayList<String> lookup = new ArrayList<String>();
    private Scanner csvReader;

    // Lakukan inisialisasi file read
    public ReadFile(String files) throws FileNotFoundException {
        try {
            this.csvReader = new Scanner(new File(files));
        
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("File "+ files +" tidak ditemukan");
            //TODO: handle exception
        }
        this.csvReader = new Scanner(new File(files));
    }

    // Memasukkan isi file kedalam sebuah arraylist of array. Anda dapat
    // memodifikasi bentuk penyimpanan sesuai kebutuhan.
    public ArrayList<String[]> putIntoList() {
        while (csvReader.hasNextLine()) {
            String line = csvReader.nextLine();
            String[] kolom = line.split(",");
            if (String.join(",",kolom).startsWith("id")==false) {
                this.temp.add(kolom);
            }
        }
        return temp;
    }
    //Lookup adalah kumpulan id yang sudah dipakai oleh customer
    public ArrayList<String> getLookup(){
        for (String[] id : temp) {
            lookup.add(id[0]);
        }
        return lookup;
    }
}
