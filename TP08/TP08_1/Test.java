import java.util.Scanner;


public class Test{
  public static void main(String[] args){
        Scanner inp = new Scanner(System.in);
        System.out.print("Ada berapa manusia: ");
        int x = inp.nextInt();
        Manusia[] inform = new Manusia[x];
        
        for(int i=0; i<x; i++){ 
            System.out.println("Siapa Anda: ");
            String status = inp.next().toLowerCase();
            
            System.out.println("Masukkan data-data: ");
            String nama = inp.next();
            double uang = inp.nextDouble();
            int umur = inp.nextInt();
            if (status.equals("pegawai")){
                String level = inp.next();
                Pegawai data = new Pegawai(nama, uang, umur, level);
                inform[i] = data;
            }else{
                Pelanggan data = new Pelanggan(nama, uang, umur);
                inform[i] = data;
            }
        }

        System.out.println("-------------------");
        System.out.println("Data-data berhasil dimasukkan! Terima kasih!");
        boolean z = true;

        while(z == true){
          System.out.print("perintah: ");
          String input = inp.nextLine();
          String[] q = input.split(" ");
          if(!input.equals("selesai")){
            try{
              int idx = 0;
              for(int i=0; i<inform.length; i++){
                if(inform[i].getNama().toLowerCase().equals(q[0].toLowerCase())){
                  if(q[1].toLowerCase().equals("bicara")){
                    System.out.println(inform[i].bicara());
                  }
                  else if(q[1].toLowerCase().equals("beli")){
                    Pelanggan obj = (Pelanggan)inform[i];
                    System.out.println(obj.beli());
                  }
                  else if(q[1].toLowerCase().equals("berjalan")){
                    System.out.println(inform[i].berjalan());
                  }
                  else{
                    System.out.println("Masukkan input dengan benar");
                  }
                }else{
                    idx += 1;
                }
              }
              if(idx == inform.length && !input.equals("")){
                System.out.println("Tidak ada yang bernama "+q[0]+"!");
              }
            }catch(Exception e){
              System.out.println("Perintah tidak dapat dilakukan");
            }
          }else{
              System.out.println("Sampai jumpa!");
              z = false;
          }
        }
  }
}