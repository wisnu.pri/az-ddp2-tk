# 💻 Tugas Pemrograman 8 (Kelompok)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) [![](https://img.shields.io/badge/SCeLE-Click%20Here-orange.svg)](https://scele.cs.ui.ac.id/course/view.php?id=3019) [![](https://img.shields.io/badge/LINE%20OA-Kak%20Burhan-brightgreen)](https://line.me/R/ti/p/%40157zmdwf)

**Kelas**:  | **Kode Tutor**: 

## Topik

They are my children. They inherited my methods and properties. Although they may behave differently. I'm still proud of them!

## Dokumen Tugas

https://docs.google.com/document/d/1NiNwX6s_BFXOXMgapfo85fH59xtbMJJPzBs1b9ip6fU/edit?usp=sharing

## Pembagian Tugas

| No.  | Nama | NPM  | Tugas |
| ---- | ---- | ---- | ----- |
| 1    |      |      |       |
| 2    |      |      |       |
| 3    |      |      |       |
| 4    |      |      |       |
| 5    |      |      |       |
| 6    |      |      |       |
| 7    |      |      |       |

## *Checklist*

- [ ] Membaca dan memahami Soal 1
- [ ] Membaca dan memahami Soal 2
- [ ] Membaca dan memahami Soal 3
- [ ] Membaca dan memahami Soal 4
- [ ] Membaca dan memahami Soal 5
- [ ] Mengerjakan Soal 1
- [ ] Mengerjakan Soal 2
- [ ] Mengerjakan Soal 3
- [ ] Mengerjakan Soal 4
- [ ] Mengerjakan Soal 5
- [ ] Melaporkan kepada asdos terkait progress pada Milestone 1
- [ ] Melaporkan kepada asdos terkait progress pada Milestone 2
- [ ] Melaporkan kepada asdos terkait progress pada Milestone 3
- [ ] Melakukan demo secara kolektif