import org.junit.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class TP08Test {
    @Test
    public void testAttributePegawai(){
        Pegawai Yoga = new pegawai("Yoga", 100.0, 20, "pemula");
        assertEquals("Yoga", Yoga.getNama());
        assertTrue(100.0, Yoga.getUang());
        assertEquals(20, Yoga.getUmur());
        assertEquals("pemula", Yoga.getLevelKeahlian());
    }
    @Test
    public void testObjectPelanggan(){
        Pelanggan Mahendra = new pelanggan("Mahendra", 100.0, 20, 0);
        assertEquals("Mahendra", Mahendra.getNama());
        assertEquals(100.0, Mahendra.getUang(),0);
        assertEquals(20, Mahendra.getUmur());
    }
    @Test
    public void testSetGetLevelKeahlian(){
        Pegawai Yoga = new Pegawai("Yoga", 100.0, 20, "pemula");
        assertEquals("pemula", Yoga.getLevelKeahlian());
        Yoga.testSetGetLevelKeahlian("mahir");
        assertEquals("mahir", Yoga.getLevelKeahlian());
    }
    @Test
    public void testPegawaiBicara(){
        assertEquals("Selamat datang di kedai!", Pegawai.bicara());
    }
    @Test
    public void testPelangganBeli(){
        assertEquals("Aku akan membeli minuman!", Pegawai.beli());
    }
    @Test
    public void testPegawaiJalan(){
        Pegawai Yoga = new Pegawai("Yoga", 100.0, 20, "pemula");
        assertEquals("Yoga sedang berjalan!", Pegawai.berjalan(Yoga.getNama())); 
    }
    @Test
    public void testPelangganJalan(){
        assertEquals("Aku sedang berjalan!", Manusia.berjalan()); 
    }
    @Test
    public void testPelangganJalanLebih(){
        int langkah = 3;
        assertEquals("Aku sedang berjalan 3 langkah!", Pelanggan.berjalan(langkah));
    }
    @Test
    public void testBandingkanBurhan(){
        assertEquals("Tidak ada yang bernama Burhan!", Manusia.tidakAdaNama("Burhan"));
    }
}
// belum kelar
