
public class Pegawai extends Manusia {
    private String levelKeahlian;

    public Pegawai(String nama, double uang, int umur, String levelKeahlian){
        setNama(nama);
        setUang(uang);
        setUmur(umur);
        this.levelKeahlian = levelKeahlian;
        
    }

    public String getLevel(){
        return this.levelKeahlian;
    }

    public String berjalan() {
        return getNama() + " sedang berjalan!";
    }

}
