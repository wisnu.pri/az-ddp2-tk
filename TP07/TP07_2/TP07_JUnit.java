import static org.junit.Assert.*; 
import org.junit.Test;


public class TestBurhan{
  @Test
  public void testJumlahDefaultUangBurhan(){ // Contoh test case
      Burhan burhan = new Burhan();
      // assertEquals ini akan true, karena default uang kak burhan 
      // adalah 10,000.0DDD
      assertEquals(burhan.getUang(),10000.0); 
  }
  
  // Template method test case
  @Test
  public void testSesuatu(){ 
	  assertEquals("a","a");
  }
}
