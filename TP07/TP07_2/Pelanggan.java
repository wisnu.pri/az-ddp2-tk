public class Pelanggan {
    // TODO: Tambahkan field yang dibutuhkan disini
    String nama;
    double uang;

    public Pelanggan(String nama,double uang){   // TODO: Lengkapi constructornya
        this.nama = nama;
        this.uang = uang;
    }
    // TODO: Lengkapi method Getter dan Setternya
    public void setUang(double uang) {
        this.uang = uang;
    }   
    public double getUang() {
        return uang;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getNama() {
        return this.nama;
    }
    public String toString() {
        return this.nama;
    }
    public void bayar(Burhan burhan,Bon bon) {
       this.uang -= bon.getHargaTotal();
    }
    public void mintaBantuan(Burhan burhan, Pelanggan p, double n) {
        burhan.bantu(p, n);
    }
}
