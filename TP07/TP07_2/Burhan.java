import java.util.ArrayList;
public class Burhan {
    String nama = "Kak Burhan";
    double uang = 10000.0;
    // TODO: Tambahkan field yang dibutuhkan disini
    public Burhan(){   // TODO: Lengkapi constructornya
    }
    public Burhan(double uang){
        this.uang = uang;
    }
    // TODO: Lengkapi method Getter dan Setternya
    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getNama() {
        return nama;
    }
    public void setUang(double uang) {
        this.uang = uang;
    }
    public Double getUang() {
        return uang;
    }
    public String toString() {
        return this.nama;
    }
    public void bantu(Pelanggan p,double jumlah){
        this.uang -= jumlah;
        p.uang += jumlah;
    }
    public void bersihkan(Restoran k) {
        k.setIdxK(100.0);
    }
    
}
