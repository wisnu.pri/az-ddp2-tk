import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * NOTE: tidak ada yang perlu diubah didalam file ini,
 * but feel free to modify the menu templates. Suit yourself :D.
 * 
 * @Usage langsung saja java Seeder
 */
public class Seeder {
    public static void main(String[] args) throws FileNotFoundException {
        seed("menu.txt");
    }

    static void seed(String filename) throws FileNotFoundException {
        PrintWriter seeder = new PrintWriter(new File(filename));
        ArrayList<String> seed = new ArrayList<>();
        ArrayList<String> exist = new ArrayList<>();
        String[] food = new String[]{
                "Nasi", "Nasi Goreng", "Nasi Bakar", 
                "Mie Goreng", "Mie Kuah",
                "Lontong", "Ketupat"
            };
        String[] condi = new String[]{
                "Ayam Goreng", "Ayam Penyet", "Ayam Geprek", "Ayam Bakar",
                "Ikan Goreng", "Ikan Bakar", "Ikan Pepes"
            };
        String[] beverages = new String[]{
            "Air Putih", "Teh", "Susu", "Kopi", "Jamu", "Jus Buah"
            };
        String[] temperature = new String[]{
            "Panas","Hangat","Es","Dingin","Biasa"
        };
        String[] prices = new String[]{
            "5000","10000","15000","20000","25000"
        };
        print("=================================");
        print("Seeder sedang membuat daftar menu");
        print("Daftar Makanan:");
        for (int i = 0; i < 10; i++) {
            String s = randContent(food) + " " + randContent(condi) + " : ";
            if(exist.contains(s)){
                continue;
            }else{
                exist.add(s);
                seed.add(s + randContent(prices));
                print(s + randContent(prices));
            }
            
        }
        print("\nDaftar Minuman:");
        for (int i = 0; i < 6; i++) {
            String s = randContent(beverages) + " " + randContent(temperature) + " : ";
            if(exist.contains(s)){
                continue;
            }else{
                exist.add(s);
                seed.add(s + randContent(prices));
                print(s + randContent(prices));
            }
        }
        print("Seeder menulis daftar menu ke file...");
        for (String s : seed) {
            seeder.println(s);
        }
        seeder.flush();
        print("Seeder selesai membuat daftar menu");
        print("=================================");

    }            
    static String randContent(String[] data){
        return data[(int) (data.length * Math.random())];
    }   
    static void print(String s){
        System.out.println(s);
    }   
}
