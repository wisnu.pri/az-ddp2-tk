import java.util.Scanner;

public class Simulator {    // NOTE: Silakan gunakan template yang diberikan sesuai keinginan dan kepercayaan masing-masing.
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Hardcoded Testcase, hasil tergantung interaksi pelanggan.
        
        Restoran voidMain = new Restoran(0.0);
        Burhan burhan = voidMain.getBurhan();
        Bon obj = new Bon();
        
        // ==============================User Input===============================================
        String perintah; String inp; boolean x = true;
        do {
            perintah = scanner.nextLine(); voidMain.hapusAntrianPelanggan();

            if (perintah.toLowerCase().startsWith("tambah antrian")){
                for(int i=0; i < Integer.parseInt(perintah.split(" ")[3]); i++){
                    inp = scanner.nextLine();

                    if(inp.toLowerCase().startsWith("tambah pelanggan")){
                        String[] data = inp.split(" ");
                        voidMain.tambahAntrianPelanggan(new Pelanggan(data[2],Double.parseDouble(data[3])));
                        System.out.println("Pelanggan " + data[2] + " datang!");
                    }
                }
                voidMain.layaniPelanggan(obj); obj.hargaTotalAll = 0.0;
            }else if (perintah.equalsIgnoreCase("Tutup Kedai")){
                voidMain.tutupRestoran(obj);
                x = false;
            }else{
                System.out.println("Masukkan input dengan benar!");
            }
        } while (x);
    }
    public static void print(String s) {
        System.out.println(s);
    }
    public static void print(int i) {
        System.out.println(i);
    }
    public static void print(double i) {
        System.out.println(i);
    }
}
