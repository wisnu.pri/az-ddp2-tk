import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Collections;

public class Bon {
    // TODO: Tambahkan field yang dibutuhkan disini
    static long BonIDCounter = 1L;
    long IDBon;
    boolean lunas;
    private ArrayList<String> y;
    ArrayList<String[]> daftarPesanan;
    double hargaTotal;
    double hargaTotalAll = 0.0;
    private HashMap<String, Integer> rekapMenu;
    HashMap<String, Integer> rekapAkhir = new HashMap<String, Integer>();

    public Bon(){
    }
    public Bon(ArrayList<String[]> daftarPesanan,double idxK){   // TODO: Lengkapi constructornya
        this.IDBon = Bon.BonIDCounter;
        this.daftarPesanan = daftarPesanan;
        this.hargaTotal = hitungHargaTotal(daftarPesanan)*(idxK/100);
    }
    // TODO: Lengkapi method Getter dan Setternya
    public void setIDBon(){
        this.IDBon = Bon.BonIDCounter;
    }
    public void setDaftarPesanan(ArrayList<String[]> daftarPesanan){
        this.daftarPesanan = daftarPesanan;
    }
    public void setHargaTotal(ArrayList<String[]> daftarPesanan, double idxK){
        this.hargaTotal = hitungHargaTotal(daftarPesanan)*(idxK/100);
    }
    public String toString(){
        hashToSortArray();
        String pesanan = "";
        for(int i=0; i < this.y.size(); i++){
            pesanan += this.y.get(i) + " " + this.rekapMenu.get(this.y.get(i));
            if(i < this.y.size()-1){
                pesanan += "\n";
            }
        }
        return pesanan;
    }
    public double hitungHargaTotal(ArrayList<String[]> daftarPesanan){
        double totalHarga = 0.0;
        for (String[] pesanan : daftarPesanan) {
            totalHarga += Double.parseDouble(pesanan[2]);
        }
        return totalHarga;
    }
    public void parseDaftarPesanan(ArrayList<String[]> daftarPesanan){
        this.daftarPesanan = daftarPesanan;
    }
    public double getHargaTotal(){
        return this.hargaTotal;
    }
    public void setRekapTotalMenu(ArrayList<String[]> rekap){
        HashMap<String, Integer> isiMenu = new HashMap<String, Integer>();
        for(String[] comp : rekap){
            if(isiMenu.get(comp[0]) == null){
                isiMenu.put(comp[0], Integer.parseInt(comp[1]));
            }else{
                isiMenu.put(comp[0], isiMenu.get(comp[0]) + Integer.parseInt(comp[1]));
            }
        }
        this.rekapMenu = isiMenu;
    }
    public HashMap<String, Integer> getRekapTotalMenu(){
        return this.rekapMenu;
    }
    public void hashToSortArray(){
        ArrayList<String> q = new ArrayList<>();
        for(String t : this.rekapMenu.keySet()){
            q.add(t);
        }
        Collections.sort(q);
        this.y = q;
    }
    public void setHargaTotalAll(){
        this.hargaTotalAll += this.hargaTotal;
    }
    public double getHargaTotalAll(){
        return this.hargaTotalAll;
    }
    public void setRekapAkhir(){
        for(String t : this.rekapMenu.keySet()){
            if(rekapAkhir.get(t) == null){
                rekapAkhir.put(t, rekapMenu.get(t));
            }else{
                rekapAkhir.put(t, rekapAkhir.get(t) + rekapMenu.get(t));
            }
            
        }
    }
    public HashMap<String, Integer> getRekapAkhir(){
        return rekapAkhir;
    }
    public String toString(HashMap<String, Integer> p){
        ArrayList<String> q = new ArrayList<>();
        for(String t : p.keySet()){
            q.add(t);
        }Collections.sort(q);

        String pesanan = "";
        for(int i=0; i < q.size(); i++){
            pesanan += q.get(i) + " " + p.get(q.get(i));
            if(i < q.size()-1){
                pesanan += "\n";
            }
        }return pesanan;
    }
}
