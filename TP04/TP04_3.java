import java.util.Scanner;

public class TP04_3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Selamat Datang di Kedai Kopi VoidMain!\nMenu Spesial Hari Ini:");
        String menu1 = sc.nextLine();
        String menu2 = sc.nextLine();
        String menu3 = sc.nextLine();

        String pil1 = terfavorit(menu1, menu2, menu3);
        String pil2 = termahal(menu1, menu2, menu3);
        String pil3 = termurah(menu1, menu2, menu3);
        String pil4 = tercepat(menu1, menu2, menu3);

        if (pil1.equals(pil3) && pil3.equals(pil4)){
            System.out.println(pil3+" Harus Banget Kamu Beli!");
        }if (pil1.equals(pil2) && pil2.equals(pil4)){
            System.out.println(pil2+" Menu Terbaik Hari Ini!");
        }
        System.out.println("Okay, Sekarang mau menu yang kaya gimana nih?");
        String salah = "";
        while(salah != "exit"){
            String pilihan = sc.nextLine().toLowerCase();
            switch (pilihan){
                case "terfavorit":
                    System.out.println("Menu Terfavorit hari ini : "+pil1); break;
                case "termahal":
                    System.out.println("Menu Termahal hari ini : "+pil2); break;
                case "termurah":
                    System.out.println("Menu Termurah hari ini : "+pil3); break;
                case "tercepat":
                    System.out.println("Menu Tercepat hari ini : "+pil4); break;
                case "exit":
                    salah = "exit"; 
                    System.out.println("Jangan bosan mampir di kedai VoidMain ya, Kak!"); break;
                default:
                System.out.println("Opsi tidak ada"); break;
            }
        }    
    }
    public static String termurah(String menu1, String menu2, String menu3){
        double hargaMenu1 = 0.3+(1*menu1.length());
        double hargaMenu2 = 0.5+(1*menu2.length());
        double hargaMenu3 = 0.7+(1*menu3.length());
        if (hargaMenu1<hargaMenu2 && hargaMenu1<hargaMenu3)
            return menu1;
        else if (hargaMenu2<hargaMenu3)
            return menu2;
        else
            return menu3;
    }
    public static String termahal(String menu1, String menu2, String menu3){
        double hargaMenu1 = 0.3+(1*menu1.length());
        double hargaMenu2 = 0.5+(1*menu2.length());
        double hargaMenu3 = 0.7+(1*menu3.length());
        if (hargaMenu1>hargaMenu2 && hargaMenu1>hargaMenu3)
            return menu1;
        else if (hargaMenu2>hargaMenu3)
            return menu2;
        else
            return menu3;
    }
    public static String terfavorit(String menu1, String menu2, String menu3) {
        //TODO : Ubah Return Value dan Parameter jika dibutuhkan
        String menu1L = menu1.toLowerCase();
        String menu2L = menu2.toLowerCase();
        String menu3L = menu3.toLowerCase();
        int[] index_menu = {menu1L.length(),menu2L.length(),menu3L.length()};
        int terpendek = 1000000000;
        for (int k = 0; k< index_menu.length; k++){
            if (index_menu[k] < terpendek){
                terpendek = index_menu[k];
            }
        }

        for (int i = 0; i < terpendek; i++) {
            if (menu1L.charAt(i) < menu2L.charAt(i) && menu1L.charAt(i) < menu3L.charAt(i)){
                return menu1;
            }else if (menu2L.charAt(i) < menu1L.charAt(i) && menu2L.charAt(i) < menu3L.charAt(i)){
                return menu2;
            }else if (menu3L.charAt(i) < menu1L.charAt(i) && menu3L.charAt(i) < menu2L.charAt(i)){
                return menu3;
            }else if(menu1L.charAt(i) == menu2L.charAt(i) && menu1L.charAt(i)<menu3L.charAt(i)){
                if (index_menu[0]<index_menu[1] && menu1.substring(0,terpendek).equals(menu2.substring(0,terpendek))){
                    return menu1;
                }else if (index_menu[0]>index_menu[1] && menu1.substring(0,terpendek).equals(menu2.substring(0,terpendek))){
                    return menu2;
                }
                for (int j = 0; j<terpendek; j++){
                    if (menu1L.charAt(j)<menu2L.charAt(j)){
                        return menu1;
                    }else if (menu1L.charAt(j)>menu2L.charAt(j)){
                        return menu2;
                    }
                }return menu1;

            }else if(menu1L.charAt(i) == menu3L.charAt(i) && menu1L.charAt(i)<menu2L.charAt(i)){
                if (index_menu[0]<index_menu[2] && menu1.substring(0,terpendek).equals(menu3.substring(0,terpendek))){
                    return menu1;
                }else if (index_menu[0]>index_menu[2] && menu1.substring(0,terpendek).equals(menu3.substring(0,terpendek))){
                    return menu3;
                }
                for (int j = 0; j<terpendek; j++){
                    if (menu1L.charAt(j)<menu3L.charAt(j)){
                        return menu1;
                    }else if (menu1L.charAt(j)>menu3L.charAt(j)){
                        return menu3;
                    }
                }return menu1;

            }else if(menu2L.charAt(i) == menu3L.charAt(i) && menu2L.charAt(i)<menu1L.charAt(i)){
                if (index_menu[1]<index_menu[2] && menu2.substring(0,terpendek).equals(menu3.substring(0,terpendek))){
                    return menu2;
                }else if (index_menu[1]>index_menu[2] && menu2.substring(0,terpendek).equals(menu3.substring(0,terpendek))){
                    return menu3;
                }
                for (int j = 0; j<terpendek; j++){
                    if (menu2L.charAt(j)<menu3L.charAt(j)){
                        return menu2;
                    }else if (menu2L.charAt(j)>menu3L.charAt(j)){
                        return menu3;
                    }
                }return menu2;
            }
        }
        return menu1;
    }
    public static String tercepat(String menu1, String menu2, String menu3) {
        int waktuPengerjaan = 10*60;                        //dalam detik
        //hitung jumlah kata
        String[] kataMenu1 = menu1.split(" ");
        String[] kataMenu2 = menu2.split(" ");
        String[] kataMenu3 = menu3.split(" ");

        int time1 = kataMenu1.length*waktuPengerjaan + 2;
        int time2 = kataMenu2.length*waktuPengerjaan + 5;
        int time3 = kataMenu3.length*waktuPengerjaan + 10;
 
        if (time1<time2 && time1<time3){
            return menu1;
        }else if (time2<time3){
            return menu2;
        }else{
            return menu3;
        }
    }
}
